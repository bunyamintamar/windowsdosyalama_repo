﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace KucukNotDefteri
{
    public partial class anaForm : Form
    {
        public anaForm()
        {
            InitializeComponent();
        }

        private void anaForm_Load(object sender, EventArgs e)
        {

        }

        private void btnDosyayaYaz_Click(object sender, EventArgs e)
        {
            FileStream fs = File.Create("C:\\Users\\Bunyamin TAMAR\\Desktop\\Küçük Notlar\\"+txtDosyaAdi.Text+".txt");
            byte[] text = new UTF8Encoding(true).GetBytes(txtNot.Text);
            fs.Write(text, 0, txtNot.Text.Length);
            fs.Close();
            MessageBox.Show("Küçük bir not alındı");
            txtDosyaAdi.Clear();
            txtNot.Clear();
        }
    }
}
