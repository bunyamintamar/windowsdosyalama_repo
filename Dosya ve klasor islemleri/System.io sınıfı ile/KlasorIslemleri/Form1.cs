﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;



namespace KlasorIslemleri
{
    public partial class anaForm : Form
    {



        String dosyaYolu = "C:\\Kucuk notlar";
        bool metinGirildi = false;
        private void dosyaOlustur(String yol, String ad, String metin)
        {
            FileStream fs = File.Create(yol + "\\" + ad + ".txt");
            byte[] text = new UTF8Encoding(true).GetBytes(metin);
            fs.Write(text, 0, text.Length);
            fs.Close();
        }

        private bool dosyaMevcutMu(String yol, String ad)
        {
            if (File.Exists(yol + "\\" + ad + ".txt"))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private void dosyaSil(String yol, String ad)
        {
            if( dosyaMevcutMu(yol,ad) )
            {
                File.Delete(yol + "\\" + ad + ".txt");
            }
        }

        private String dosyadanOku(String yol, String ad)
        {
            String okunan;
            StreamReader sr = new StreamReader(yol + "\\" + ad + ".txt");
            okunan = sr.ReadToEnd();
            sr.Close();
            return okunan;
        }

        private void dialogDosyaOku()
        {
            DialogResult dialogButon;
            dialogButon = dosyaAgaciniAc.ShowDialog();
            if (dialogButon == DialogResult.OK)
            {
                string dosya = System.IO.Path.GetFileNameWithoutExtension(dosyaAgaciniAc.FileName);
                txtKonu.Text = dosya;
                txtMetin.Text = dosyadanOku(dosyaYolu, dosya);
            }
        }

        private void basligiTemizle()
        {
            txtKonu.Text = "";
        }

        private void konuyuTemizle()
        {
            txtMetin.Text = "";
        }

        private void temizle()
        {
            basligiTemizle();
            konuyuTemizle();
        }
        public anaForm()
        {
            InitializeComponent();
        }

        private void anaForm_Load(object sender, EventArgs e)
        {
            if (!Directory.Exists(dosyaYolu))
            {
                MessageBox.Show("Küçük notlara hoşgeldin :)\n\nNotların burada olacak:\nC:\\Kucuk notlar","",MessageBoxButtons.OK);
                Directory.CreateDirectory(dosyaYolu);
            }
        }

        private void btnKaydet_Click(object sender, EventArgs e)
        {

            if( txtKonu.Text == "" )
            {
                MessageBox.Show("Başlık yazmayı unuttun.\nBaşlık olmadan kaydedilemez!");
            }
            else
            {
                dosyaSil(dosyaYolu, txtKonu.Text);
                txtMetin.Text = txtMetin.Text + "  ("+DateTime.Now+")";
                dosyaOlustur(dosyaYolu, txtKonu.Text, txtMetin.Text);
                temizle();
                lblKayitDurumu.Text = "KÜÇÜK BİR NOT ALINDI :)";
                metinGirildi = false;
            }
        }

        private void dialogKayit_FileOk(object sender, CancelEventArgs e)
        {

        }

        private void txtKonu_TextChanged(object sender, EventArgs e)
        {
            lblKayitDurumu.Text = "";
            if (dosyaMevcutMu(dosyaYolu, txtKonu.Text))
            {
                mevcutNotUyarisi.Text = "böyle bir not var!";
                if (txtMetin.Text == "")
                {
                    txtMetin.Text = dosyadanOku(dosyaYolu, txtKonu.Text);
                }
            }
            else
            {
                mevcutNotUyarisi.Text = "";
            }
        }

        private void btnNotlariGoster_Click(object sender, EventArgs e)
        {
            dialogDosyaOku();
        }

        private void txtMetin_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
