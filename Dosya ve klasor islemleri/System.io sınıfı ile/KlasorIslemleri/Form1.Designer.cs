﻿namespace KlasorIslemleri
{
    partial class anaForm
    {
        /// <summary>
        ///Gerekli tasarımcı değişkeni.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///Kullanılan tüm kaynakları temizleyin.
        /// </summary>
        ///<param name="disposing">yönetilen kaynaklar dispose edilmeliyse doğru; aksi halde yanlış.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer üretilen kod

        /// <summary>
        /// Tasarımcı desteği için gerekli metot - bu metodun 
        ///içeriğini kod düzenleyici ile değiştirmeyin.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(anaForm));
            this.btnKaydet = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.txtMetin = new System.Windows.Forms.RichTextBox();
            this.txtKonu = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.dialogKayit = new System.Windows.Forms.SaveFileDialog();
            this.lblKayitDurumu = new System.Windows.Forms.Label();
            this.btnNotlariGoster = new System.Windows.Forms.Button();
            this.dosyaAgaciniAc = new System.Windows.Forms.OpenFileDialog();
            this.mevcutNotUyarisi = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnKaydet
            // 
            this.btnKaydet.Location = new System.Drawing.Point(12, 174);
            this.btnKaydet.Name = "btnKaydet";
            this.btnKaydet.Size = new System.Drawing.Size(75, 23);
            this.btnKaydet.TabIndex = 2;
            this.btnKaydet.Text = "Kaydet";
            this.btnKaydet.UseVisualStyleBackColor = true;
            this.btnKaydet.Click += new System.EventHandler(this.btnKaydet_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Başlık:";
            // 
            // txtMetin
            // 
            this.txtMetin.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMetin.Location = new System.Drawing.Point(12, 72);
            this.txtMetin.Name = "txtMetin";
            this.txtMetin.Size = new System.Drawing.Size(400, 96);
            this.txtMetin.TabIndex = 1;
            this.txtMetin.Text = "";
            this.txtMetin.TextChanged += new System.EventHandler(this.txtMetin_TextChanged);
            // 
            // txtKonu
            // 
            this.txtKonu.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtKonu.Location = new System.Drawing.Point(12, 25);
            this.txtKonu.Name = "txtKonu";
            this.txtKonu.Size = new System.Drawing.Size(400, 20);
            this.txtKonu.TabIndex = 0;
            this.txtKonu.TextChanged += new System.EventHandler(this.txtKonu_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 56);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(73, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Küçük bir not:";
            // 
            // dialogKayit
            // 
            this.dialogKayit.FileOk += new System.ComponentModel.CancelEventHandler(this.dialogKayit_FileOk);
            // 
            // lblKayitDurumu
            // 
            this.lblKayitDurumu.AutoSize = true;
            this.lblKayitDurumu.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblKayitDurumu.ForeColor = System.Drawing.Color.Green;
            this.lblKayitDurumu.Location = new System.Drawing.Point(93, 179);
            this.lblKayitDurumu.Name = "lblKayitDurumu";
            this.lblKayitDurumu.Size = new System.Drawing.Size(0, 13);
            this.lblKayitDurumu.TabIndex = 3;
            // 
            // btnNotlariGoster
            // 
            this.btnNotlariGoster.Location = new System.Drawing.Point(337, 174);
            this.btnNotlariGoster.Name = "btnNotlariGoster";
            this.btnNotlariGoster.Size = new System.Drawing.Size(75, 23);
            this.btnNotlariGoster.TabIndex = 4;
            this.btnNotlariGoster.Text = "Notlar";
            this.btnNotlariGoster.UseVisualStyleBackColor = true;
            this.btnNotlariGoster.Click += new System.EventHandler(this.btnNotlariGoster_Click);
            // 
            // dosyaAgaciniAc
            // 
            this.dosyaAgaciniAc.InitialDirectory = "C:\\Kucuk notlar";
            this.dosyaAgaciniAc.Title = "Kayıtları bir notu aç";
            // 
            // mevcutNotUyarisi
            // 
            this.mevcutNotUyarisi.AutoSize = true;
            this.mevcutNotUyarisi.ForeColor = System.Drawing.Color.Red;
            this.mevcutNotUyarisi.Location = new System.Drawing.Point(327, 9);
            this.mevcutNotUyarisi.Name = "mevcutNotUyarisi";
            this.mevcutNotUyarisi.Size = new System.Drawing.Size(0, 13);
            this.mevcutNotUyarisi.TabIndex = 5;
            // 
            // anaForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(426, 205);
            this.Controls.Add(this.mevcutNotUyarisi);
            this.Controls.Add(this.btnNotlariGoster);
            this.Controls.Add(this.lblKayitDurumu);
            this.Controls.Add(this.txtKonu);
            this.Controls.Add(this.txtMetin);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnKaydet);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "anaForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Küçük bir not";
            this.Load += new System.EventHandler(this.anaForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnKaydet;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RichTextBox txtMetin;
        private System.Windows.Forms.TextBox txtKonu;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.SaveFileDialog dialogKayit;
        private System.Windows.Forms.Label lblKayitDurumu;
        private System.Windows.Forms.Button btnNotlariGoster;
        private System.Windows.Forms.OpenFileDialog dosyaAgaciniAc;
        private System.Windows.Forms.Label mevcutNotUyarisi;
    }
}

